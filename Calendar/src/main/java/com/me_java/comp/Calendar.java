package com.me_java.comp;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import net.miginfocom.swing.MigLayout;

public class Calendar extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel pane;
	private JLabel lb;
	private JTextField tf;
	private JDateChooser dc;

	public Calendar() {
		super("Calendar");
//		setTitle("Calendar");
		setLocation(500, 200);
		setSize(300, 200);
		setResizable(false);
		
		prepareLayout();
		addCompAction();
		
		setVisible(true);
	}

	public void prepareLayout() {
		lb = new JLabel("Your Birthday : ");
		tf= new JTextField();
		dc = new JDateChooser();
		
		// Layout, Column, Row
		pane = new JPanel(new MigLayout("CENTER", "", "20[]50[]"));
		pane.add(dc, "gapx 20, spanx 2, width 150::150, height 25::25, wrap");
		pane.add(lb);
		pane.add(tf, "width 100::100, height 25::25");
		
		add(pane);
	}

	public void addCompAction() {
		dc.addPropertyChangeListener("date", new PropertyChangeListener() {

			public void propertyChange(PropertyChangeEvent e) {
				SimpleDateFormat sdf =new SimpleDateFormat("MMM d, YYYY");
				String date = sdf.format(e.getNewValue());
				tf.setText(date);
			}
			
		});
	}

	public static void main(String[] args) {
		new Calendar();
	}

}
